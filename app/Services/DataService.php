<?php

namespace App\Services;

use App\Exel\ImportArticles;
use Illuminate\Support\Facades\Http;
use Maatwebsite\Excel\Facades\Excel;

class DataService
{
    public string $route = 'https://techcrunch.com/wp-json/wp/v2/posts?per_page=30&order=desc';
    public array $result = [];
    public int $countArticles = 0;

    public function getData($page = 1)
    {
        $articles = json_decode(Http::get($this->route . '&page=' . $page)->body());

        foreach ($articles as $article) {
            $this->countArticles++;
            $this->result[] = [
                'title' => $article->title->rendered,
                'content' => $article->content->rendered,
            ];

            if ($this->countArticles === 140) {
                break;
            }
        }

        if ($this->countArticles === 140) {
            return $this->result;
        }
        $page++;

        return $this->getData($page);
    }

    public function getDataFromExel()
    {
        $rows = Excel::toArray(new ImportArticles(), request()->file('file'));

        foreach ($rows[0] as $row) {
            $data[] = [
                'title' => $row[0],
                'content' => $row[1],
                'date' => $row[2],
            ];
        }
        usort($data, function ($a, $b) {
            return strtotime($b['date']) - strtotime($a['date']);
        });

        return $data;
    }
}
