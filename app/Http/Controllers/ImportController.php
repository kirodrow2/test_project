<?php

namespace App\Http\Controllers;

use App\Exel\ExportArticles;
use App\Http\Requests\FileUploadRequest;
use App\Services\DataService;
use Maatwebsite\Excel\Facades\Excel;

class ImportController extends Controller
{
    public function index()
    {
        return view('main');
    }

    public function fileUpload(FileUploadRequest $request, DataService $apiService)
    {
        if ($request->hasFile('file')) {
            $data = $apiService->getDataFromExel();
        } else {
            $data = $apiService->getData();
        }

        if ($request->format_file === 'exel') {
            return Excel::download(new ExportArticles($data), 'articles.xlsx');
        } else {
            if ($request->format_file === 'csv') {
               return Excel::download(new ExportArticles($data), 'articles.csv', \Maatwebsite\Excel\Excel::CSV);
            } else {
                return Excel::download(new ExportArticles($data), 'articles.csv', \Maatwebsite\Excel\Excel::CSV);
            }
        }
    }
}
