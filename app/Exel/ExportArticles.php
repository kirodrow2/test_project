<?php

namespace App\Exel;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;

class ExportArticles implements FromCollection
{
    private array $data;

    public function __construct($data)
    {
        $this->data = $data;
    }


    public function collection()
    {
        return new Collection($this->data);
    }
}
